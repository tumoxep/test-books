import Books from './components/Books.vue'
import NotFound from './components/NotFound.vue'
import InternalServerError from './components/InternalServerError.vue'

import store from './store'

const routes = [
  { 
    name: 'books',
    path: '/reader/:userId/:shelfStr?/:filterStr?',
    component: Books,
    props: true,
    beforeEnter: (to, from, next) => {
      store.commit('user/setUserId', to.params.userId)
      store.commit('filters/setCurrentFilters', to.params.shelfStr)
      store.commit('filters/setActiveFilter', to.params)
      store.dispatch('shelves/loadUserShelves').then(
        response => {
          if (typeof to.params.shelfStr !== 'undefined') {
            store.commit('shelves/setActive', to.params.shelfStr)
            next()
          } else {
            next({ name: to.name, params: { ...to.params, shelfStr: response.body[0].slug }})
          }
        },
        error => {
          if (error.status === 404) {
            next({ name: '404' })
          } else {
            next({ name: '500' })
          }
        }
      )
    }
  },
  { 
    path: '/404', 
    name: '404', 
    component: NotFound, 
  },
  { 
    path: '/500', 
    name: '500', 
    component: InternalServerError, 
  },
  { path: '/', redirect: '/reader/1/#shelf1' },
  { path: '*', redirect: '/404' }
]

export default routes