const state = {
  id: null
}

const mutations = {
  setUserId (state, id) {
    state.id = id
  }
}

export default {
  namespaced: true,
  state,
  mutations
}