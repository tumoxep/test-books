import _ from 'lodash'

import { 
  SHELF_READED, RATING_FILTERS, USER_FILTERS,
  ID_FILTER_KEY, CREATED_FILTER_KEY } from '../../AppConstants'

const state = {
  ratingFilters: RATING_FILTERS,
  userFilters: USER_FILTERS,
  currentFilters: null,
  activeFilter: null,
  reverse: true
}

const mutations = {
  setCurrentFilters (state, forShelf) {
    if (_.includes(forShelf, SHELF_READED)) {
      state.currentFilters = state.ratingFilters
    } else {
      state.currentFilters = state.userFilters
    }
  },
  setActiveFilter (state, params) {
    if (!params.filterStr) {
      if (_.includes(params.shelfStr, SHELF_READED)) {
        state.activeFilter = ID_FILTER_KEY
      } else {
        state.activeFilter = CREATED_FILTER_KEY
      }
    } else {
      if (params.filterStr[0] === '-') {
        state.activeFilter = params.filterStr.slice(1)
        state.reverse = true
      } else {
        state.activeFilter = params.filterStr
        state.reverse = false
      }
    }
  },
  setReverse (state, val) {
    state.reverse = val
  },
  toggleReverse (state) {
    state.reverse = !state.reverse
  }
}

export default {
  namespaced: true,
  state,
  mutations
}