import _ from 'lodash'
import Vue from 'vue'

import { SHELF_READED } from '../../AppConstants'

const state = {
  userShelves: [],
  ratingShelf: {
    id: -1,
    name: 'Прочитанные книги',
    slug: '#readed1'
  },
  activeShelfIdx: null,
  loadedPage: null,
  allLoaded: false,
  shelf: [],
  perPage: null
}

const getters = {
  allShelves: (state) => {
    return _.concat(state.userShelves, state.ratingShelf)
  }  
}

const actions = {
  loadUserShelves ({ commit, rootState }) {
    return new Promise((resolve, reject) => {
      Vue.apiService.getUserShelves(rootState.user.id).then(response => {
        if (response.body.length) {
          commit('setUserShelves', response.body)
          resolve(response)
        } else {
          reject('empty');
        }
      }, error => {
        reject(error);
      })
    })
  },
  loadShelf ({ commit, state, rootState }, type) {
    return new Promise((resolve, reject) => {
      if (type === 0) {
        Vue.apiService.getUserShelf({
          user: rootState.user.id,
          bookshelf: state.userShelves[state.activeShelfIdx].id,
          o: (rootState.filters.reverse ? '-' : '') + rootState.filters.activeFilter,
          page: state.loadedPage + 1
        }).then(
          response => {
            if (response.body.objects.length > 0) {
              if (!state.loadedPage) {
                commit('setPerPage', response.body.per_page)
              }
              commit('setLoadedPage', state.loadedPage + 1)
              commit('pushBooksToShelf', response.body.objects)
            } else {
              commit('setAllLoaded')
            }
            resolve(response)
          },
          error => { reject(error) }
        ) 
      } else {
        Vue.apiService.getRatingShelf({
          user: rootState.user.id,
          o: (rootState.filters.reverse ? '-' : '') + rootState.filters.activeFilter,
          page: state.loadedPage + 1
        }).then(
          response => {
            if (response.body.objects.length > 0) {
              commit('setLoadedPage', state.loadedPage + 1)
              commit('pushBooksToShelf', response.body.objects)
            } else {
              commit('setAllLoaded')
            }
            resolve(response)
          },
          error => { reject(error) }
        ) 
      }
    })
  }
}

const mutations = {
  setUserShelves (state, shelves) {
    state.userShelves = shelves
  },
  setActive (state, forShelf) {
    if (_.includes(forShelf, SHELF_READED)) {
      state.activeShelfIdx = state.userShelves.length;    
    } else {
      const idx = _.findIndex(state.userShelves, (el) => { return el.slug === forShelf; });
      state.activeShelfIdx = idx
    }
  },
  setLoadedPage (state, num) {
    state.loadedPage = num
  },
  pushBooksToShelf (state, books) {
    state.shelf = state.shelf.concat(books)
  },
  setAllLoaded (state) {
    state.allLoaded = true
  },
  clearShelf (state) {
    state.shelf = []
  },
  resetLoadedPage (state) {
    state.loadedPage = 0
  },
  resetAllLoaded (state) {
    state.allLoaded = false
  },
  setPerPage (state, num) {
    state.perPage = num
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}