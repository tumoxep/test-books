import Vue from 'vue'
import Vuex from 'vuex'

import shelves from './modules/shelves'
import user from './modules/user'
import filters from './modules/filters'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    shelves,
    user,
    filters
  },
  mutations: {
  }
})