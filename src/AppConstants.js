export const IMG_SERVER = 'http://my-LIB.ru'
export const SHELF_READED = 'readed'
export const RATING_FILTERS = {
  'id': { text: 'По дате добавления' },
  'score': { text: 'По оценке' }
}
export const USER_FILTERS = {
  'created': { text: 'По дате добавления' },
  'work__default_edition__title': { text: 'По названию' },
  'work__rating_score': { text: 'По общему рейтингу' }
}
export const ID_FILTER_KEY = 'id'
export const CREATED_FILTER_KEY = 'created'
