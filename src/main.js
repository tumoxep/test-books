import Vue from 'vue'
import VueRouter from 'vue-router'
import VueResource from 'vue-resource'

import App from './App.vue'
import routes from './AppRoutes'
import store from './store'
import apiService from './services/ApiService'

Vue.config.productionTip = false

const router = new VueRouter({
  routes,
  mode: 'history'
})
Vue.use(VueRouter)

Vue.use(VueResource)

Vue.use(apiService)

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
