import Vue from 'vue'

const API_USERBOOKS = '/api/userbooks/';
const API_RATING = '/api/rating/';
const API_BOOKSHELF = '/api/userbooks/bookshelf';

class ApiService {
    getUserShelf(params) {
        return Vue.http.get(
            API_USERBOOKS,
            { params }
        )
    }

    getRatingShelf(params) {
        return Vue.http.get(
            API_RATING,
            { params }
        )
    }

    getUserShelves(userId) {
        return Vue.http.get(
            API_BOOKSHELF,
            { 
                params: {
                    user: userId
                }
            }
        )
    }
}

export default {
    install: function (Vue) {
        Vue.apiService = new ApiService();
    }
}
