module.exports = {
    "devServer":{
        "proxy": {
            "/api": {
                "target": 'http://my-lib.ru',
                "changeOrigin": true,
                "secure": false
            }
        }
    }
  }