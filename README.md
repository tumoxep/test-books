# Тестовое задание

## Запуск

### Standalone

```shell
npm install
npm run serve
```

### Docker

```shell
docker build -t <имя контейнера> .
docker run -p 8080:8080 <имя контейнера>
```
